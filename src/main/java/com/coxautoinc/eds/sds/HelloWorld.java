package com.coxautoinc.eds.sds;

import com.coxautoinc.eds.common.DefaultValue;
import org.apache.deltaspike.cdise.api.CdiContainer;
import org.apache.deltaspike.cdise.api.CdiContainerLoader;
import org.apache.deltaspike.cdise.api.ContextControl;
import org.apache.deltaspike.core.api.provider.BeanProvider;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by akgillella on 7/25/16.
 */
public class HelloWorld {

    @Inject
    @DefaultValue(key="greeting.name", value = "myName")
    private String name;

    @Inject
    @DefaultValue(key="greeting.message", value = "message")
    private String message;


    @PostConstruct
    public void greet(){

        System.out.println(message+"  "+name);
    }


    public static void main(String[] args){

        // CDI container initialization
        CdiContainer cdiContainer = CdiContainerLoader.getCdiContainer();
        cdiContainer.boot();
        ContextControl contextControl = cdiContainer.getContextControl();
        contextControl.startContexts();


        HelloWorld helloWorld = BeanProvider.getContextualReference(HelloWorld.class);


    }

}
